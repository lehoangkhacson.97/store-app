import React, { useContext, useState } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import _ from 'lodash'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Ionicons } from '@expo/vector-icons'
import { withBadge } from 'react-native-elements'

import { Cart, Orders, Settings, Category, Categories } from './src/screens'
import AppContext from './AppContext'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const CategoryStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Home' component={Categories} />
      <Stack.Screen
        name='Category'
        component={Category}
        options={({ route }: any) => ({ title: route.params.categoryName })}
      />
    </Stack.Navigator>
  )
}

const CartStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Cart' component={Cart} />
    </Stack.Navigator>
  )
}
const OrdersStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Orders' component={Orders} />
    </Stack.Navigator>
  )
}
const SettingsStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Settings' component={Settings} />
    </Stack.Navigator>
  )
}

const AppNavigator = () => {
  const { cartItems } = useContext(AppContext)
  const badge =  _.reduce(cartItems, (count: number, cartItem: any) => {
    return count + cartItem.quantity
  }, 0)
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName
          if (route.name === 'Home') {
            iconName = 'ios-information-circle'
          }
          if (route.name === 'Settings') {
            iconName = 'ios-list'
          }
          if (route.name === 'Orders') {
            iconName = 'ios-apps'
          }
          if (route.name === 'Cart') {
            iconName = 'ios-cart'
          }
          const Icon = route.name === 'Cart' && badge > 0 ? withBadge(badge)(Ionicons) : Ionicons

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />
        }
      })}
    >
      <Tab.Screen name='Home' component={CategoryStack} />
      <Tab.Screen name='Cart' component={CartStack} />
      <Tab.Screen name='Orders' component={OrdersStack} />
      <Tab.Screen name='Settings' component={SettingsStack} />
    </Tab.Navigator>
  )
}

export default AppNavigator
