import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { StyleSheet, FlatList } from 'react-native'

import CategoryListItem from '../components/CategoryListItem'

export default function Categories({ navigation }) {
  const [categories, setCategories] = useState([])

  useEffect(() => {
    axios.get('/category')
      .then((res: any) => {
        setCategories(res.data)
      })
      .catch((error: any) => {
        console.log(error)
      })
  }, [])

  const handlePress = (item: any) => () => {
    navigation.navigate('Category', {
      categoryName: item.name,
      categoryId: item.id
    })
  }

  return (
    <FlatList
      data={categories}
      renderItem={({ item }) => <CategoryListItem category={item} onPress={handlePress(item)}/>}
      contentContainerStyle={styles.container}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingTop: 16
  },
  scrollView: {}
})
