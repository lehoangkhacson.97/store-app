import React, { useContext } from 'react'
import { StyleSheet, Text, View, Alert } from 'react-native'

import CartListItem from '../components/CartListItem'
import AppContext from '../../AppContext'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function Cart(props) {
  const appContext = useContext(AppContext)
  const { cartItems, addToCart, removeFromCart, getTotal, removeAllFromCart } = appContext

  const total = getTotal() ? `${getTotal()} K` : ''

  const handlePay = () => {
    removeAllFromCart()
    Alert.alert('Thanh toán thành công!')
  }

  return (
    <View style={styles.container}>
      <View style={styles.categories}>
        {cartItems.length > 0 &&
          cartItems.map((cartItem: any, index: number) => (
            <CartListItem
              key={index}
              product={cartItem.product}
              addToCart={addToCart}
              removeFromCart={removeFromCart}
              quantity={cartItem.quantity}
            />
          ))}
      </View>
      <View style={styles.total}>
        <Text style={styles.price}>{total}</Text>
        <TouchableOpacity onPress={handlePay}>
          <Text style={styles.pay}>Đặt Hàng</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '100%'
  },
  categories: {},
  total: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 8,
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#fff',
    padding: 8,
    shadowColor: '#000',
    shadowRadius: 10,
    shadowOpacity: 0.1,
    shadowOffset: { height: 0, width: 0 }
  },
  price: {
    flex: 1,
    fontSize: 18,
    fontWeight: '600'
  },
  pay: {
    backgroundColor: '#4ba9fc',
    padding: 16,
    fontSize: 18,
    color: '#fff',
    borderRadius: 4
  }
})
