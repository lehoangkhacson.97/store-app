import Cart from './Cart'
import Orders from './Orders'
import Settings from './Settings'
import Category from './Category'
import Categories from './Categories'

export { Cart, Orders, Settings, Category, Categories }
