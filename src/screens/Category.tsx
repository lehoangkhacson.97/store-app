import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import { StyleSheet, Text, View, Alert } from 'react-native'

import ProductListItem from '../components/ProductListItem'
import { FlatList } from 'react-native-gesture-handler'
import AppContext from '../../AppContext'

export default function Category({ route }) {
  const { categoryId } = route.params
  const appContext = useContext(AppContext)
  const { addToCart } = appContext

  const [products, setProducts] = useState([])

  useEffect(() => {
    axios.get(`/products?category=${categoryId}`)
      .then((res: any) => {
        setProducts(res.data)
      })
      .catch((error: any) => {
        console.log(error)
      })
  }, [])

  const onAddToCartClick = (product: any) => {
    addToCart(product)
    Alert.alert(`Đã thêm sản phẩm ${product.name} vào giỏ hàng!`)
  }

  return (
    <FlatList
      data={products}
      numColumns={2}
      contentContainerStyle={styles.container}
      renderItem={({ item }: any) => (
        <View style={styles.wrapper}>
          <ProductListItem
            product={item}
            onAddToCartClick={() => onAddToCartClick(item)}
          />
        </View>
      )}
      keyExtractor={(item: any) => `${item.id}`}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
    paddingTop: 16
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: 8
  }
})
