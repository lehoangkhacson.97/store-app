import React from 'react'
import {
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'

import ItemImage from '../../assets/item.png'
import { Category } from '../types'

interface Props {
  category: Category
  onPress: any
}

export default function CategoryListItem(props: Props) {
  const {
    category: { name },
    onPress
  } = props
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={onPress}>
      <View style={styles.container}>
        <Text style={styles.title}>{name}</Text>
        <Image source={ItemImage} style={styles.categoryImage} />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 16,
    margin: 16,
    borderRadius: 4,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 0 },
    marginBottom: 8
  },
  title: {
    textTransform: 'uppercase',
    marginBottom: 8,
    fontWeight: '700'
  },
  categoryImage: {
    width: 64,
    height: 64
  }
})
