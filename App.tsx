import React, { useState, createContext } from 'react'
import axois from 'axios'
import _ from 'lodash'
import { StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'

import AppNavigator from './AppNavigator'
import AppContext from './AppContext'

axois.defaults.baseURL = 'http://localhost:3000'

export default function App() {
  const [cartItems, setCartItems] = useState([])

  const addToCart = (product: any) => {
    const index = _.findIndex(
      cartItems,
      (cartItem: any) =>
        cartItems && cartItem.product && product.id === cartItem.product.id
    )
    if (index !== -1) {
      setCartItems([
        ...cartItems.slice(0, index),
        {
          ...cartItems[index],
          quantity: cartItems[index].quantity + 1
        },
        ...cartItems.slice(index + 1)
      ])
      return
    }
    setCartItems([
      ...cartItems,
      {
        product,
        quantity: 1
      }
    ])
  }

  const removeFromCart = product => {
    const index = _.findIndex(
      cartItems,
      cartItem =>
        cartItem && cartItem.product && product.id === cartItem.product.id
    )
    if (index !== -1) {
      if(cartItems[index].quantity === 1) {
        setCartItems([
          ...cartItems.slice(0, index),
          ...cartItems.slice(index + 1)
        ])
        return
      }
      setCartItems([
        ...cartItems.slice(0, index),
        {
          ...cartItems[index],
          quantity: cartItems[index].quantity - 1
        },
        ...cartItems.slice(index + 1)
      ])
      return
    }
    return
  }

  const getTotal = () => {
    return _.reduce(
      cartItems,
      (total: number, cartItem: any) => {
        console.log(cartItem)
        const { product, quantity } = cartItem
        const price = parseInt(product.price) * quantity
        return total + price
      },
      0
    )
  }

  const removeAllFromCart = () => {
    setCartItems([])
  }

  const context = {
    cartItems,
    addToCart,
    removeFromCart,
    getTotal,
    removeAllFromCart
  }

  return (
    <AppContext.Provider value={context}>
      <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
    </AppContext.Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'center'
  }
})
